module LineGrape  
  class API < Grape::API
  	client = Rails.application.config.line_client
    post do
	  body = request.body.read

	  signature = request.env['HTTP_X_LINE_SIGNATURE']
	  unless client.validate_signature(body, signature)
	    error 400 do 'Bad Request' end
	  end

	  events = client.parse_events_from(body)

	  events.each { |event|
	    puts "Event: "
	    puts event
	    case event
	    when Line::Bot::Event::Message
	      puts "Message: "
	      puts event.type
	      case event.type
	      when Line::Bot::Event::MessageType::Text
	      	# SHOULD BE A WORKER
	      	a = /\[(.*?)\]/.match(event.message['text']	)
	      	
	      	if a.nil?
	      		Message.create(:text => event.message['text'])
	      	else
	      		puts "a is not nil #{a[0]}"
	      		Message.create(:color =>  a[0].delete('[]').downcase, :text => event.message['text'].sub(a[0], ''))
	      	end
	      	# END OF WHAT WORKER SHOULD BE
	        message = {
	          type: 'text',
	          text: event.message['text']
	        }
	        client.reply_message(event['replyToken'], message)
	      end
	    end
	  }

	  "OK"
	end
  end
end  
