module MsgDatabase
  class API < Grape::API
  	format :json
  	get "" do
	    { 
	    	id: Message.last.id,
	    	color: Message.last.color,
	    	text: Message.last.text
	    }
	  end
  end
end  
