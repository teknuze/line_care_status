Rails.application.routes.draw do
  get 'stat_page/index'
  mount LineGrape::API => '/'
  mount MsgDatabase::API => '/msg'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
