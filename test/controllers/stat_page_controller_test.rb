require 'test_helper'

class StatPageControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get stat_page_index_url
    assert_response :success
  end

end
